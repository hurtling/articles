Training a class of compassionate scientist engineers, whose education is 75% engineering, 25% social history and the arts, would in my opinion create a group of people with the heart to want to help people, and the knowledge to actually do something useful.

(Most) of the science must be human-centric; there is no point understanding the universe until we have learned to feed ourselves. Both are noble goals worth achieving, but only one will kill you if you don't do it.

That being said, there is nothing wrong with speculative research, which may lead somewhere interesting to us. LASERs were originally thought of as expensive toys, and now they are everywhere.

We are just intelligent animals, and science must keep that in mind at all times. No one is exempt from jealousy, pride, tribalism, fear-mongering and the rest. What is the point in colonising Mars if we just bring our unsolved social problems with us?

We don't need emotionless, borderline-aspergers robots who'd work on murder machines as happily as on a cure for cancer, as long as the problem was interesting.

We also don't need hand-wringing liberal arts majors with no understanding of how anything actually works, and a fear/distrust of the underlying technology they rely on.

We need a synthesis of these two groups. A person needs both a heart and a brain.

People with professional interest in neither the arts nor sciences can become part of the invaluable foundation of society, the Basics: food growth, education, cleaning, childcare, roadbuilding, bin-emptying. Without these, nothing else would work. They can partially be considered however, 'solved problems'; things we have learned a successful method for doing, which don't need messing around with. Education might still need tweaking though.

Are there really any 'solved problems' though? what about food growth? we need to continue raising the density/efficiency at which we grow food, in order to feed everyone as the population expands. That cannot remain static. Road building? uses Bitumen, a by-product of the oil industry. We'll need another way to build roads when the oil runs out. Bin-emptying hasn't even been perfected yet; not everything is recycled. We are still sweeping tons of rubbish under the proverbial carpet of landfill, and trying to forget about it.

A government which takes money from its citizens (taxes them) without doing anything good for them (providing services) is completely useless, and possibly corrupt.

%tags: politics,ideas
