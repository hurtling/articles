Star Trek Re-Do
=====

no transporters - they just make no sense, and ruin perfectly good stories, remove threat.  
site-to-site replication might exist, beaming the things energy and 'blueprint',  
but this could easily act as a weapon, given the energy content of matter  

gay and trans people exist!

no holodeck - however a matrix-like neural simulator exists, but is controversial  
tactile button-based controls for priority posts  
replicators exist, but require supplies of raw materials  
many people prefer to cook their own food, replicating the ingredients instead

forcefields are limited, and have not replaced airlocks  
orbital mechanics are respected, as are the newtonian laws of motion  
planetary exploration is carried out by specially-trained away-teams (rather than bridge crew),  
launched in drop-ships

articial gravity exists as a passive, solid-state technology: that is, it works even when unpowered.

Major command decisions should require the consensus of at least 2 of the 3 senior officers.  
In fact, the command structure should be less militaristic in general

A Multi-Species Civilisation
---------
The UFP's success is due to fusing the pursuit of scientific knowledge with compassion for fellow people.

### Interpersonal


The Federation is by definition a post-scarcity civilisation; people don't NEED to work, 
but it is encouraged that everyone contributes to society in a way that pleases them, and/or according to their ability.

People show a great deal more goodwill and willingness to help others than in the West now.

Having large crews man starships has implications that are not positive; it implies that the societty has (and must have) 'little people'; people of little achievement (implied to be of little worth, 'stepped on') who must do relatively menial or undesirable jobs.
Gene Roddenberry's original goal was to have technology remove the NEED for people 'empty the bins', as it were (or scrub the plasma manifolds etc).
A better setting would be a much smaller crew, the bulk of which consitute the central cast. No nameless extras.


### Between Species

due to logistical constraints, starships are usually only single- or dual-species environments, so that their atmospheres, temperatures, humidities, aromas etc can be tailored to the comfort of their crew.

Dual-species ships have the advantage of also acting as long-term exchange programs, 
allowing members of each species to get to know each other by working alongside each other a real-world environment,
rather than in the phony politeness of ambassadorial missions.

Mixed-species crews, while sometimes facing major logistical problems 
(materials which have desirable mechanical properties in all temperature ranges its species are comfortable in?) , are also better able to handle challenges which would otherwise overwhelm members of a particular species. In the same way the Voyager's doctor (and TNG's Data) act as non-biological backups when everything goes wrong, one species would not be vulnerable to the same things as the other.

### Between Civilisations

What if there were other multi-species civilisations, with high-minded goals not dissimilar to the Federation itself;
yet they and the Federation have tense relations (possibly due to a disagreement on some obscure political principles,
a la the tendency of the Left to schism)?

The idea being, to not have simple straw-man enemies that it's obvious we're supposed to dislike.  
'Enemies' should have a viewpoint that is understandable (or at least partially) yet misguided, or for which there are people out there today who believe the same.

* use fewer-to-no examples of an american metaphor -- no big, heavily-armed military organisation with thinly-veiled colonial tendencies
    - no "it's for your own good" and gunboat diplomacy
* replicators can assemble relatively simple shapes from existing materials -- but they can't perform alchemy; they need the right raw materials, as elemental fusion is prohibitively costly of energy, and probably wasteful

Warp Drive and FTL Travel
-------
Warp drive exists because it has to, for the premise of an intersolar civilisation to work at all.

It's a riff on the alcubierre drive theory, or whatever seems plausible and fashionable in physics now.

the UFP operates as an 'Asynchronous Society': because of the time dilation effects of warp drive 
(which may be less drastic than in real life, but still present to some extent),
solar systems act isolated pockets of local time.

Going on a far intersolar mission means 50 or more years will have passed back home; this is why starship crew bring their family along!

AI and Trans-Humanism
------
Artifical life has been banned, after a war for survival which nearly wiped out all biological life  
computers and large computing machines are allowed, but neural nets are forbidden,  
	and small electronics are allowed on a case-by-case basis  
along with this, electronic body modification is an extreme taboo(maybe?)  


Realism of Details
-----
* Seatbelts. Much cheaper than inertial dampeners, and saves on cheesy 'bump' acting
* People have to finish announcing their intended recipient before a comm badge connects them to that recipient
* Stop referring to Starfleet as "Humans", or acting like everyone in starfleet knows all about Earth history
* No more tokenboobs catsuit & pushup bra crewmembers; just put the same characters in a normal uniform!
* People don't all know how to use all computers
* Other species may have multiple cultures: pacifist klingons, emotional vulcans...
* there aren't just energy weapons & shields -- sometimes kinetic weapons work better.

Increased Scientific Accuracy
----

### General

* Use real units and materials where possible -- no gigaquads, instead gigabytes; no isorems but rather millisieverts.
* Equally, stop inventing a new form of radiation every week as a throwaway plot-point
* No wave or beam can penetrate a planet, without also destroying that planet
* Heat dissipation and deceleration are still challenges 
* R-squared law must still be respected -- for all things, like explosions, sounds, point-sources
    - parabolic dishes should also come into play at least as much as they already do -- ie the Deflector Dish

### Space Travel

* There are crazy, enterprise-like starship designs, and then there are those suitable for atmospheric travel.
* A ship with big sticky-out nacelles on thin pylons will always suffer unnecessary structural stresses when turning at speed;
    - structural forcefields would help, but only with enough power to run them.  
More sensible designs should always win out  

### Medicine

* Individuals are not suddenly and irrevocably dead when a machine makes a persistent whining noise
* Also stop using the 80s medical drama of defibrillator usage


Key technological advances:
----
manipulation of gravity  
highly efficient reaction drives (needing far less fuel than anything today)  
warp drive (an extension of the gravitational technology)  
