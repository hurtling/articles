Mandolin Chords
=====

I could probably find this information on the internet somewhere, but working these out is good practice for me.


For Solo Play
-------

Where the bass note is the first or the fifth of the chord. These chords can be played on their own.

X		| Maj  | min  | Maj7 | min7 | Dom7
--------|------|------|------|------|-----
A		| 2240 | 2230 | 2244 | 2233 | 2243
A#/Bb	| 3011 | 	  |      |      |
B		|      | 4022 |      | 4002 | 4102
C		| 0230 | 0133 |      |      |
C#/Db	| 1341 | 1240 |      |      |
D		| 2002 | 2001 | 2042 | 2031 | 2032
D#/Eb	|      |      |      |      |
E		| 4224 | 4223 |      |      |
F		| 5301 |      |      |      |
F#/Gb	|      |      |      |      |
G		| 0023 | 0013 | 0022 | 0011 | 0021
G#/Ab	|      |      |      |      |

For Accompanied Play
-------

Where the bass note can be any part of the chord. We rely on the bass note from another instrument (such as bass guitar).

X       | Maj  | min  | Maj7 | min7 | Dom7
--------|------|------|------|------|-----
A		| 2240 | 2230 | 2244 | 2233 | 2243
A#/Bb	| 3011 |      |      |      |
B		| 4122 | 4022 |      | 4002 | 2122
C		| 0230 | 0133 | 4220 |      |
C#/Db	| 1341 | 1240 |      |      |
D		| 2002 | 2001 | 2042 | 2031 | 2032
D#/Eb	| 0113 | 0112 |      |      |
E		| 1220 | 0220 | 1120 | 0020 | 1020
F		| 5301 |      | 2330 | 1131 | 2131
F#/Gb	|      |      |      |      |
G		| 0023 | 0013 | 0022 | 0011 | 0021
G#/Ab	| 1134 | 1124 | 1133 | 1122?| 1132?

%tags: music
