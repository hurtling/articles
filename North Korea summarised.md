North Korea
to summarise what I've 'learned' in 2.5 days of documentaries;

Hardship and starvation are both rife, partly due to international trade embargoes, but also due to:-

* an incompetent government letting ideology come before prosperity;
* a very corrupt bureaucratic class siphoning off large portions of the country's wealth;
* wasting the 10 best years of every citizen's life prancing about in uniform instead of working to improve the country (food, research and production). Not to mention the wealth that has been redirected to pay for the military hardware

The US is partly causing the famine, and many western media agencies don't mind pointing at the resulting problems and either laughing at the country or showing how much of a failure communism is, as they did (and still do) about the USSR.

However, the reason they are doing this to NK is because the US are trying to starve out the nation and cause a governmental collapse and revolution from within, without directly meddling with the country using military action (which would cause a bloody war, making Japan and South Korea casualties).

All in all, it seems like the North Koreans have been truly brainwashed to love their leaders and their country (rather like US citizens but more so - flag in every classroom?). In their spare time, they love music and performance (which must be about how awesome NK/Their Great Leader is, but they don't seem to mind), physical sports (which are often cheap to do), and try to passionately put their all into everything they do.

The whole world is waiting for them to fail (and subtly tries to make it happen without being too obvious), and they know it. They know they can only rely on themselves, and a little (decreasing) bit on China. As a result, they want to show everyone how much they have achieved on their own, and everyone there is determined to do their absolute best, the proverbial 110%, to prove the rest of the world wrong.

The constant war-talk is just butthurt about being victimised; it also helps to make the US into an even bigger scapegoat than it actually is a problem, and keep the public on the establishment's side.

The stuff about Kim Jong Il being the Greatest Human Ever are just the insecurities writ large of a dicatorial idiot.

It has lost its two greatest allies (USSR 20 years ago in the 1990s, which caused horrible starvation by 1994) and China recently, after Hu Jintao hinted at Jong-Un's uncle to make it into more of a Chinese-style 'socialism' (state-run market economy). He was killed by Jong Un in order to solidify his loyalty base, the Special Economic Zone in Rason has, I don't know, been shut down? and even China are sick of their shit.

NK will head further and further into a cult of personality drained by its leader's corruption. Because they've already got an excuse on standby for any more catastrophes, I think the whole area would starve to death before overthrowing JU.

If the thing about the gift economy buying loyalty is true, then perhaps a slighted minor government official could wield enough power to get rid of the Big Un (I just made that up), but it's likely the situation would then descend into a Congo-like insane bloodbath of a power struggle, but with more missiles.

The authoritarion police state stuff doesn't seem to bother them much, as it didn't in the USSR and doesn't in the USA, UK and China. The state spying on everyone just seems to be a fact of life. Everywhere now.

Best thing that can happen to NK is that the Kim Jong line is wiped out and a politburo thingy of the usual communist old codgers in massive hats takes over, and gradually turns the country into a china-like economy, as Jong-Un's uncle tried to do. Also accepting foreign aid would be a start, but then the insane militaristic nationalism (not to mention the underlying pride) would need to be somehow dealt with first.

From NK's perspective, the UN is biased against them, and considering its president is currently South Korean, they may even be right. The South Koreans really do hate the North with as passionate a fervour (dressed in civilised Western sentiment) as the North hate them. They both want to save each other from the error of their ways at the end of a gun(sort of pitying their wayward cousins), but sometimes don't think they're worth all this effort and would be better off wiped off the face of the earth.

Solutions
=========

They would be best off learning as much as they can about modern science and engineering; solving their power problems with modernised versions of the traditional communal farms: communal Wind Farms!

Their complete lack of modern infrastructure (partly caused by an oil shortage down to US embargoes, partly down to the Kim Jongs' IDGAF attitude to minimum living standards) has in the past been considered a major sign of the country's failure to provide for its citizens; this liability could be turned into an asset.

They could sidestep the entire industrial revolution and brand themselves a green economy. Tourists wishing to see a country untouched by industrialisation (well that may be a lie, considering communism's traditional heavy emphasis on heavy industry) could see an entire Country of Outstanding Natural Beauty. If they want to boast about something, they could boast about their independence from legacy oil-based technology (ie cars, which they were never allowed to properly get on board with in the first place except as status symbols for party members, but that's not the point!)

Electrified bicycles and quadricycles could provide a post-industrial travel network for the populace; localised electricity generation using small wind and solar installations would remove the need for extensive power infrastructure; travellers could charge their vehicles at these stations. One power hub per town, perhaps. Existing centralised power generation needs could be shorn up by enhancing the hydropower capacity of the Great Barrage thing they built on the mouth of the Taedong.

They could probably never catch up with China's and Japan's microchip manufacturing industries, but then neither can anyone else; they will probably never become an Asian Tiger economy (unless they do it in 50+ years' time, when the former exploited manufacturing countries like India have themselves become too 'civilised' for dirty work). But they could define themselves as something completely unique.
