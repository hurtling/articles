Non-Industry Games ("Nondy")
======

* Day 1 mod API
	- mods are required (by license agreement) to be open-source, allowing other players to pick up the torch of abandoned but popular mods
* NO LOOTBOXES
* Early access release only when all game mechanics have been completed
	- another way of saying it: no new game-changing mechanics will be implemented after EA release
* DLCs:
	- expansion packs may be released, capped at <50% the cost of the base game each
* Weekly polls (with something like uservoice.com) about what development to prioritise
	- which features/bugfixes/optimisations 
* restrictions on early access
	- Current price is proportional to the amount the game is finished
	- no working on or releasing DLC until the base game is finished
	- time in EA
	- if game development becomes abandoned before completion, the source code is released into the public domain