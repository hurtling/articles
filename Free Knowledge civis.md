a copying-based society, where everyone can see everyone else's work
all outside-the-brain memory is shared, which reduces wasted, duplicated effort 
people check other people's work
a log of education they consume is kept, whch forms their qualifications
their competence to check others is based on their qualifications

any major decision by a person is not justified by their fame, wealth, persuasiveness or power
but instead by an exam which backs up that they are sufficiently qualified to make that decision
