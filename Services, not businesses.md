The problem with a lot of businesses are that they are actually services, which People rely on.
Those businesses, are 'owned' by small groups of selfish individuals, whose primary goal is to make as much
profit as possible by whatever means, using whatever they have access to.
This is usually directly at odds with the function of the Service.

These 'owners' are never usually users of the service, as their wild success in this runaway 
capitalist age means they use their vast wealth to provide themselves with a private equivalent of that service,
which isn't subject to the arbitrary changes they themselves impose on the service in the name of profit.

No single private selfish interest should have the right to buy, sell or otherwise dispose of that Service.
There needs to be a class of profitable organisation, a Service, which is not allowed to shut down
without reasonable, exceptional circumstances, which must be thoroughly explained to their users,
and whose validity should be decided by the courts.
They should be held accountable to be public which uses that service,
and 'owned' by a separate interest than runs it (to prevent such selfishness from occurring naturally),
which is itself bound by unbreakable terms to provide the service no matter what.

The service must not be broken. The service cannot be bought or sold.
The service is not subject to the whims of individuals, only to the people who need it.
The service CAN make (a reasonable* amount of) money, can charge money for its services.

But their prices must remain at the same price as (adjusting for inflation, and cost of materials needed to the run the service)
when they first became popular* (over x many users, whatever number is calculated to be reasonable)
Any significant changes in price apart from those reasons (or the way the services are provided) should be examined by an independent body which can refuse the change from occuring, 
if there Insufficient Grounds for Change


Due to social changes
or advances in technology,

* Improvements to the service?
* Changes in the usefulness of the service
* Evolving the service provided into new technology (as the BBC have done repeatedly)


the owners of every company should be accessible by law
companies are not people, and should have no rights of their own
if they do have rights, they should be imprisonable (prevented from trading whatsoever)
any attempts by its employees to doctor court evidence will be treated as extreme contempt of the court by both the employees and the company itself, and will result in MASSIVE fines


any and every patent application should be examined by an independent expert in the field ofits application,
to ensure that it is not obvious, fake and is otherwise able to be protected temprorarily by patent law.

A government's entire reason for being is to protect its citizens, and to ensure their prosperity (and the country's)
by providing care for them to grow into productive individuals. Any government which does not perform these basic obligations is not a government, and should not be treated as such.


It is instead just another Unaccountable Organisation of Public Influence, a classification which many global corporate empires fall into.
