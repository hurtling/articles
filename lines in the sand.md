stop drawing lines in the sand.  
stop declaring people to be your enemy,  
whether because of their religion, skin colour, political beliefs or sexual identity  

it doesn't help anyone  
intolerant people just keep being intolerant if you cut them out of your life  
you need to work with them  
talk to them  
find out why they hate  
whether ignorance, fear, a grudge or just cruelty  
help them be more tolerant

%tags: thoughts
