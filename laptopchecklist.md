# Laptop Checklist

Laptops always have something wrong with them, probably because of patents or something, or maybe just because HCI designers can't sit still, or stop reinventing the wheel (Windows 8, anyone? or perhaps Unity? or even Gnome 3?)

Here is a list I'm actively compiling, of things to check for before buying a laptop, to avoid buying dodgy ones.

A lot of these have been discovered from falling for these, so hopefully over time this list will become more comprehensive (or hopefully not), and I should buy less shoddy laptops.

### Keyboard 


  * Is the home-row intact?
      * is there a 3-wide, 2-down set of keys arranged thus:
      * insert, home, pgup
      * delete, end, pgdn?
  * are the arrow keys squashed, weirdly placed, or crowded by other extraneous keys?
  * is the keyboard British?
      * shift-3 = £
      * enter key is 2 rows tall
      * left of Z = \\
      * left of 1 = `
  * Are the function keys accessible without Fn?
  * Is the CTRL key in the bottom-left of the keyboard?
  * Are there any caps-lock, num-lock, or scroll-lock indicators?
  * (scroll lock and can be missing, that's fine. If there's no numpad, it makes sense)
  * How badly scrunched up are the arrow keys?
  * Is it easy to turn the monitor brightness up/down, WiFi and Bluetooth on-off?
      * each action should be doable with one hand, but without being able to accidentally do them

%tags: lists,hardware,practical
